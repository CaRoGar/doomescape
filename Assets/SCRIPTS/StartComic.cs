﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class StartComic : MonoBehaviour
{ 
    public VideoPlayer videoFile;
    public double currentTime;

    void Start()
    {
        videoFile = GetComponent<UnityEngine.Video.VideoPlayer>();
        StartCoroutine(PlayVideo());
    }


    void Update()
    {
        currentTime = videoFile.time;
        Debug.Log(videoFile.time);
    }

    IEnumerator PlayVideo()
    {
        yield return new WaitForSeconds(0.1f);
        videoFile.Play();
    }
}
