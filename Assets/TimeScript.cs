﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScript : MonoBehaviour
{
    public GUIText SelfText;

    public float Hours;
    public int Days;
    public int Years;

    public int Jump;
    public bool canSkip;

    //RockScript roca;
    //GirlPlayerScript chica;
    //BoyPlayerScript chico;

    GameController controller;

    void Start()
    {
        SelfText = GetComponent<GUIText>();
        canSkip = true;

        Jump = 0;
        controller = GameObject.Find("GameObject").GetComponent<GameController>();

        Hours = 0.0f;
        Days = Mathf.Clamp(Days, 0, 365);
        Years = Mathf.Clamp(Years, 1900, 2000);

        //roca = GameObject.Find("Roca").GetComponent<RockScript>();
        //chica = GameObject.Find("Chica").GetComponent<GirlPlayerScript>();
        //chico = GameObject.Find("Chico").GetComponent<BoyPlayerScript>();
    }

    void Update()
    {
        //SelfText.text = Hours.ToString("F0") + " / " + Days.ToString("F0") + " / " + Years.ToString("F0");

        SelfText.text = Years.ToString("F0");
        SelfText.fontSize = 90;

        Hours = Mathf.Clamp((Hours += 100.0f * Time.deltaTime), 1.0f, 24.0f);

        if (Input.GetKeyDown(KeyCode.L))
        {
            if ((controller.SaltoANivel2Bueno == true || controller.SaltoANivel2Malo == true) && canSkip == true)
            {
                StartCoroutine(TimeSkip1());
            }

            if (controller.SaltoANivel3Bueno == true || controller.SaltoANivel3MuyBueno == true || controller.SaltoANivel3Malo == true || controller.SaltoANivel3MuyMalo == true)
            {
                StartCoroutine(TimeSkip1());
            }
        }

        if (Hours == 24.0f)
        {
            Hours -= 24.0f;
            Days++;
        }

        if (Days == 365)
        {
            Days -= 365;
            Years++;
        }
    }

    IEnumerator TimeSkip1()
    {
        Jump += 1;

        yield return new WaitForSecondsRealtime(1.0f);

        canSkip = false;

        yield return new WaitForSecondsRealtime(999.0f);
    }
}
