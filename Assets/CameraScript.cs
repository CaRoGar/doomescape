﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public GameObject Chico;
    public GameObject Chica;
    public GameObject HouseInterior;

    void Start()
    {
        gameObject.transform.position = new Vector3(Mathf.Clamp(gameObject.transform.position.x, -3.735f, -10.93f), 0.0f, -10.0f);
    }

    void Update()
    {
        if (HouseInterior.activeSelf == false)
        {
            if (Chico.activeSelf)
            {
                gameObject.transform.position = new Vector3(Mathf.Clamp(Chico.transform.position.x, -11.0f, -3.67f), 0, -10.0f);
            }

            if (Chica.activeSelf)
            {
                gameObject.transform.position = new Vector3(Mathf.Clamp(Chica.transform.position.x, -11.0f, -3.67f), 0, -10.0f);
                //gameObject.transform.position = new Vector3(Chica.transform.position.x, 0, -10.0f);
            }
        }
    }


}
