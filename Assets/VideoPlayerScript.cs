﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoPlayerScript : MonoBehaviour
{
    public bool isVideoFinished;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PlayVideo());
        isVideoFinished = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator PlayVideo()
    {
        yield return new WaitForSeconds(35.0f);
        isVideoFinished = true;
    }
}
