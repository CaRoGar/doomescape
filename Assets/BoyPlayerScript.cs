﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoyPlayerScript : MonoBehaviour
{
    TextGuideScript guidescript;
    GameController controller;
    TimeScript timescriptscript;

    public SpriteRenderer RendererSprite;
    public Animator animator;

    private Vector3 moveDirection;

    public GameObject HouseInterior;

    public bool BirdConversation;
    public GameObject BirdMessage;

    public GameObject MensajeYayo;
    public GameObject Carta2;

    public bool Semillasbool;
    public bool ArbolPlantado;
    public bool HachaConseguida;
    public bool PicoConseguido;
    public bool AzadaConseguida;
    public bool MineralExtraído;
    public GameObject Pajaro;

    public GameObject Treee;
    public bool ArbolTalado;
    public bool SemillasPlantado;
    public GameObject Mineral;

    public bool WheatCropHarvested;
    public bool WheatCropGathered;

    public GameObject LayerTree;

    void Start()
    {
        timescriptscript = GameObject.Find("GameObject11").GetComponent<TimeScript>();
        controller = GameObject.Find("GameObject").GetComponent<GameController>();
        guidescript = GameObject.Find("TextGuides").GetComponent<TextGuideScript>();

        RendererSprite = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        BirdConversation = false;

        MensajeYayo.SetActive(true);
        Carta2.SetActive(false);

        Semillasbool = false;
        ArbolPlantado = false;
        HachaConseguida = false;
        PicoConseguido = false;
        AzadaConseguida = false;
        MineralExtraído = false;
        ArbolTalado = false;
        SemillasPlantado = false;

        WheatCropHarvested = false;
        WheatCropGathered = false;
    }

    void Update()
    {
        if ((Input.GetAxisRaw("Horizontal") > 0))
        {
            if (HouseInterior.activeSelf == true && Input.GetKey(KeyCode.Z))
            {
                gameObject.transform.Translate(Vector3.right * 4 * Time.deltaTime, Space.World);
                animator.SetInteger("condition", 2);
                RendererSprite.flipX = false;

            }
            else if (HouseInterior.activeSelf == true && !Input.GetKey(KeyCode.Z))
            {
                gameObject.transform.Translate(Vector3.right * 2.5f * Time.deltaTime, Space.World);
                animator.SetInteger("condition", 1);
                RendererSprite.flipX = false;
            }

            if (HouseInterior.activeSelf == false && Input.GetKey(KeyCode.Z))
            {
                gameObject.transform.Translate(Vector3.right * 4 * Time.deltaTime, Space.World);
                animator.SetInteger("condition", 2);
                RendererSprite.flipX = false;

            }
            else if (HouseInterior.activeSelf == false && !Input.GetKey(KeyCode.Z))
            {
                gameObject.transform.Translate(Vector3.right * 2.5f * Time.deltaTime, Space.World);
                animator.SetInteger("condition", 1);
                RendererSprite.flipX = false;
            }

        }
        else if ((Input.GetAxisRaw("Horizontal") < 0))
        {
            if (HouseInterior.activeSelf == true && Input.GetKey(KeyCode.Z))
            {
                gameObject.transform.Translate(Vector3.left * 4 * Time.deltaTime, Space.World);
                animator.SetInteger("condition", 2);
                RendererSprite.flipX = true;

            }
            else if (HouseInterior.activeSelf == true && !Input.GetKey(KeyCode.Z))
            {
                gameObject.transform.Translate(Vector3.left * 2.5f * Time.deltaTime, Space.World);
                animator.SetInteger("condition", 1);
                RendererSprite.flipX = true;
            }

            if (HouseInterior.activeSelf == false && Input.GetKey(KeyCode.Z))
            {
                gameObject.transform.Translate(Vector3.left * 4 * Time.deltaTime, Space.World);
                animator.SetInteger("condition", 2);
                RendererSprite.flipX = true;

            }
            else if (HouseInterior.activeSelf == false && !Input.GetKey(KeyCode.Z))
            {
                gameObject.transform.Translate(Vector3.left * 2.5f * Time.deltaTime, Space.World);
                animator.SetInteger("condition", 1);
                RendererSprite.flipX = true;
            }
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            animator.SetInteger("condition", 0);

        }
        else if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            animator.SetInteger("condition", 0);
        }

        if (gameObject.transform.localScale.x > 0.50f)
        {
            RendererSprite.sortingOrder = 52;
        }
        else if (gameObject.transform.localScale.x < 0.50f)
        {
            RendererSprite.sortingOrder = 8;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Respawn")
        {
            HouseInterior.SetActive(false);
            gameObject.transform.position = new Vector3(3.1f, -3.52f, 0);
            gameObject.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
            //guidescript.SelfText.text = "Press space to go in...";
            Treee.SetActive(true);
        }

        if (collision.gameObject.name == "Yayo01")
        {
            MensajeYayo.SetActive(true);
            MensajeYayo.transform.position = new Vector3(-10.59f, -0.7f, 0);
        }

        if (collision.gameObject.name == "Hacha")
        {
            Destroy(collision.gameObject);
            HachaConseguida = true;
        }

        if (collision.gameObject.name == "Manojo_trigo(Clone)")
        {
            Destroy(collision.gameObject);
            WheatCropGathered = true;
        }

        if (collision.gameObject.name == "Pico(Clone)")
        {
            Destroy(collision.gameObject);
            PicoConseguido = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "ColliderTrigo")
        {
            guidescript.SelfText.transform.position = new Vector3(0.44f, 0.06f, -10);

            if (SemillasPlantado == true && controller.SaltoANivel2Bueno == true && timescriptscript.Jump == 1)
            {
                guidescript.SelfText.text = "Press G to harvest the wheat crops";

            }
        }

        if (other.gameObject.name == "Pajaro")
        {
            StartCoroutine(BirdNarrative());
        }


        if (other.gameObject.name == "Semillas")
        {
            Semillasbool = true;
            Destroy(other.gameObject);
        }

        if (other.gameObject.name == "Azada(Clone)")
        {
            Destroy(other.gameObject);
            AzadaConseguida = true;
        }

        if (other.gameObject.name == "Carta")
        {
            Carta2.SetActive(true);
        }

        if (other.gameObject.name == "Casa_principal")
        {
            guidescript.SelfText.transform.position = new Vector3(0.8821802f, 0.03652f, -10);
            guidescript.SelfText.text = "Press space to go in...";
        }

        if (other.gameObject.name == "Mineral")
        {
            guidescript.SelfText.transform.position = new Vector3(0.17f, 0.23f, -10);
            guidescript.SelfText.text = "Press G to extract the minerals...";
        }

        if (other.gameObject.name == "Pantalla01_capa03")
        {
            guidescript.SelfText.transform.position = new Vector3(0.44f, 0.06f, -10);

            if (AzadaConseguida == true)
            {
                guidescript.SelfText.text = "Press G to plant the seeds";

            }
        }

        if (other.gameObject.name == "Arbol")
        {
            guidescript.SelfText.transform.position = new Vector3(0.44f, 0.06f, -10);

            if (Semillasbool == true && controller.Level1Layer1.name == "Pantalla01_capa01" && timescriptscript.Jump == 0)
            {
                guidescript.SelfText.text = "Press G to plant a tree sapling";
            }

            if (HachaConseguida == true && timescriptscript.Jump == 1 && ArbolPlantado == true)
            {
                guidescript.SelfText.text = "Press Space to chop down the tree";
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.name == "Pantalla01_capa03")
        {
            if (AzadaConseguida == true)
            {
                if (Input.GetKeyDown(KeyCode.G))
                {
                    SemillasPlantado = true;
                    guidescript.SelfText.text = " ";


                    Destroy(other.GetComponent<BoxCollider2D>());
                }
            }
        }

        if (other.gameObject.name == "Mineral")
        {
            {
                if (PicoConseguido == true)
                {
                    if (Input.GetKeyDown(KeyCode.G) && MineralExtraído == false)
                    {
                        guidescript.SelfText.text = " ";
                        Mineral.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Mineral_minado");
                        MineralExtraído = true;
                    }
                }
            }
        }

        if (other.gameObject.name == "Arbol")
        {
            if (Semillasbool == true)
            {
                if (Input.GetKeyDown(KeyCode.G))
                {
                    guidescript.SelfText.text = " ";
                    ArbolPlantado = true;
                    Semillasbool = false;
                }
            }

            if (controller.SaltoANivel2Bueno == true || controller.SaltoANivel2Malo == true)
            {
                if (HachaConseguida == true)
                {
                    if (Input.GetKeyDown(KeyCode.Space) && ArbolPlantado == true && ArbolTalado == false)
                    {
                        guidescript.SelfText.text = " ";
                        Treee.transform.rotation = Quaternion.Euler(0, 0, -70);
                        ArbolTalado = true;
                    }
                }
            }
        }

        if (other.gameObject.name == "ColliderTrigo")
        {
            if (Input.GetKeyDown(KeyCode.G) && SemillasPlantado == true && controller.SaltoANivel2Bueno == true)
            {
                guidescript.SelfText.text = " ";
                WheatCropHarvested = true;
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Yayo01")
        {
            MensajeYayo.SetActive(false);
        }

        if (collision.gameObject.name == "Respawn")
        {
            guidescript.SelfText.text = " ";
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Carta")
        {
            Carta2.SetActive(false);
        }

        if (collision.gameObject.name == "Casa_principal")
        {
            guidescript.SelfText.text = " ";
        }

        if (collision.gameObject.name == "Pantalla01_capa03")
        {
            guidescript.SelfText.text = " ";
        }

        if (collision.gameObject.name == "Arbol")
        {
            guidescript.SelfText.text = " ";
        }

        if (collision.gameObject.name == "Mineral")
        {
            guidescript.SelfText.text = " ";
        }

        if (collision.gameObject.name == "ColliderTrigo")
        {
            guidescript.SelfText.text = " ";
        }
    }

    IEnumerator BirdNarrative()
    {
        BirdMessage.SetActive(true);
        BirdMessage.transform.position = new Vector3(-1.44f, 0.5f, 0);

        if (Pajaro != null)
        {
            BirdMessage.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Crow_text_1");

            yield return new WaitForSecondsRealtime(4.0f);

            BirdMessage.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Crow_text_2");

            yield return new WaitForSecondsRealtime(4.0f);

            BirdMessage.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Crow_text_3");

            yield return new WaitForSecondsRealtime(4.0f);

            BirdMessage.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Crow_text_4");

            yield return new WaitForSecondsRealtime(4.0f);

            BirdMessage.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Crow_text_5");

            yield return new WaitForSecondsRealtime(4.0f);

            BirdMessage.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Crow_text_6");

            yield return new WaitForSecondsRealtime(4.0f);

            BirdMessage.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Crow_text_7");

            yield return new WaitForSecondsRealtime(4.0f);

            Destroy(BirdMessage);
            Destroy(Pajaro);

            yield break;
        }
    }
}
