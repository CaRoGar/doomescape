﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnteringHouse : MonoBehaviour
{

    public GameObject Level1Layer1;
    public GameObject Level1Layer2;
    public GameObject Level1Layer3;
    public GameObject Level1Layer4;
    public GameObject Level1Layer5;
    public GameObject Level1Layer6;

    public GameObject Level1Layer1HouseInterior;

    public GameObject ChicoPlayer;
    public GameObject ChicaPlayer;

    public GameObject Treee;

    void Start()
    {
        Level1Layer1HouseInterior.SetActive(false);
    }

    
    void Update()
    {
        
    }


    private void OnTriggerStay2D(Collider2D other)
    {
        if ((other.gameObject.name == "Chica") || (other.gameObject.name == "Chico"))
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Level1Layer1.SetActive(false);
                Level1Layer2.SetActive(false);
                Level1Layer3.SetActive(false);
                Level1Layer4.SetActive(false);
                Level1Layer5.SetActive(false);
                Level1Layer6.SetActive(false);

                gameObject.SetActive(false);

                Treee.SetActive(false);

                Level1Layer1HouseInterior.SetActive(true);
                Level1Layer1HouseInterior.transform.position = new Vector3(-3.5f, 0.2f, 0);

                if (ChicaPlayer.activeSelf)
                {
                    ChicaPlayer.transform.position = new Vector3(0, 0, 0);
                    ChicaPlayer.transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);

                }
                else if (ChicoPlayer.activeSelf)
                {
                    ChicoPlayer.transform.position = new Vector3(0, 0, 0);
                    ChicoPlayer.transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
                }
            }
        }
    }
}
