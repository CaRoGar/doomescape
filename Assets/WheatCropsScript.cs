﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheatCropsScript : MonoBehaviour
{
    //GirlPlayerScript girlplayerscriptt;
    //BoyPlayerScript boyplayerscriptt;

    TimeScript timescriptscript;

    public GameObject Wheat;
    public bool CanHarvest;

    public GameObject PantallaComprobación;

    void Start()
    {
        //girlplayerscriptt = GameObject.Find("Chica").GetComponent<GirlPlayerScript>();
        //boyplayerscriptt = GameObject.Find("Chico").GetComponent<BoyPlayerScript>();

        timescriptscript = GameObject.Find("GameObject11").GetComponent<TimeScript>();

        CanHarvest = true;
    }

    void Update()
    {

    }

    IEnumerator CanHarvestCoroutine()
    {
        yield return new WaitForSeconds(0.1f);

        CanHarvest = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Chica")
        {
            CanHarvest = true;
        }

        if (collision.gameObject.name == "Chico")
        {
            CanHarvest = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Chica")
        {
            if (Input.GetKeyDown(KeyCode.G))
            {
                GameObject WheatObject;
                WheatObject = Instantiate(Wheat, new Vector3(gameObject.transform.position.x, (gameObject.transform.position.y + 0.5f), 0.0f), Quaternion.identity) as GameObject;
                WheatObject.GetComponent<SpriteRenderer>().sortingOrder = 300;
                WheatObject.AddComponent<Rigidbody2D>().AddForce(new Vector2(-0.5f, 1.0f) * 250);
                WheatObject.AddComponent<PolygonCollider2D>();

                StartCoroutine(CanHarvestCoroutine());
            }
        }

        if (collision.gameObject.name == "Chico")
        {
            if (Input.GetKeyDown(KeyCode.G))
            {
                GameObject WheatObject;
                WheatObject = Instantiate(Wheat, new Vector3(gameObject.transform.position.x, (gameObject.transform.position.y + 0.5f), 0.0f), Quaternion.identity) as GameObject;
                WheatObject.GetComponent<SpriteRenderer>().sortingOrder = 300;
                WheatObject.AddComponent<Rigidbody2D>().AddForce(new Vector2(-0.5f, 1.0f) * 250);
                WheatObject.AddComponent<PolygonCollider2D>();

                StartCoroutine(CanHarvestCoroutine());
            }
        }
    }
}
