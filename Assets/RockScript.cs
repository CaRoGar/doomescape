﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockScript : MonoBehaviour
{
    public Camera thisCam;
    public Vector3 pos;

    public GameObject Chico;
    public GameObject Chica;

    public GameObject Suelo;
    public GameObject WaterSplash;

    public GameObject WaterParticles;

    public GameObject Box;

    public bool RiverIsBlocked;

    public GameObject Pickaxe;

    public 

    // Start is called before the first frame update
    void Start()
    {
        pos = new Vector3(0.0f, 0.0f, 0.0f);
        WaterSplash.SetActive(false);
        WaterParticles.SetActive(false);
        RiverIsBlocked = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            pos = Input.mousePosition;
            pos.z = 10;

            //Debug.Log(thisCam.ScreenToWorldPoint(pos));

            gameObject.transform.position = thisCam.ScreenToWorldPoint(pos);
        }


        if (Chico.activeSelf)
        {
            Physics2D.IgnoreCollision(Chico.GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());

        } else if (Chica.activeSelf)
        {
            Physics2D.IgnoreCollision(Chica.GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());
        }      
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Pantalla01_capa02")
        {
            StartCoroutine(SpurtGeneration());

            WaterParticles.SetActive(true);

            StartCoroutine(SplashCoroutine());
            StartCoroutine(ParticlesCoroutine());

            Physics2D.IgnoreCollision(Suelo.GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());       
        }
    }

    IEnumerator SpurtGeneration()
    {
        yield return new WaitForSeconds(1.3f);
        WaterSplash.SetActive(true);
        RiverIsBlocked = true;
        
    }

    IEnumerator SplashCoroutine()
    {
        yield return new WaitForSeconds(2.5f);
        WaterSplash.SetActive(false);
    }

    IEnumerator ParticlesCoroutine()
    {
        yield return new WaitForSeconds(3.0f);
        WaterParticles.SetActive(false);

        GameObject BoxWood;
        BoxWood = Instantiate(Box, new Vector3(-1.16f, -4.78f, 0.0f), Quaternion.identity) as GameObject;
        BoxWood.GetComponent<SpriteRenderer>().sortingOrder = 200;
        BoxWood.AddComponent<Rigidbody2D>().AddForce(new Vector2(-0.5f, 1.0f) * 250);

        yield return new WaitForSeconds(0.5f);

        BoxWood.AddComponent<PolygonCollider2D>();

        yield return new WaitForSeconds(0.5f);

        GameObject PickaxeObjectIron;
        PickaxeObjectIron = Instantiate(Pickaxe, new Vector3(BoxWood.transform.position.x, BoxWood.transform.position.y, BoxWood.transform.position.z), Quaternion.identity) as GameObject;

        Destroy(BoxWood);

        PickaxeObjectIron.GetComponent<SpriteRenderer>().sortingOrder = 210;
        PickaxeObjectIron.AddComponent<PolygonCollider2D>();



        Destroy(gameObject);
    }
}
