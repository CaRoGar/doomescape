﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuPlayButton : MonoBehaviour
{
    VideoPlayerScript Video;
    public bool GameStart;

    void Start()
    {
        gameObject.transform.position = new Vector3(0, 1.08f, 0);
        gameObject.transform.localScale = new Vector3(0.237f, 0.237f, 0.237f);

        Video = GameObject.Find("Video").GetComponent<VideoPlayerScript>();
        gameObject.SetActive(false);

        GameStart = false;
    }


    void Update()
    {
        if (Video.isVideoFinished == true)
        {
            gameObject.SetActive(true);        
        }
    }

    void OnMouseOver()
    {
        if (Video.isVideoFinished == true)
        {
            gameObject.transform.localScale = new Vector3(0.35f, 0.35f, 0.35f);

            if (Input.GetMouseButtonDown(0))
            {
                GameStart = true;
            }
        }
    }

    void OnMouseExit()
    {
        if (Video.isVideoFinished == true)
        {
            gameObject.transform.localScale = new Vector3(0.237f, 0.237f, 0.237f);
        }
    }
}
