﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[RequireComponent(typeof(AudioSource))]
public class GameController : MonoBehaviour
{
    TimeScript timeScriptscript;
    public Camera CameraFinalScreen;

    VideoPlayerScript Video1;
    public GameObject TransitionGameObject;
    public AudioSource BackgroundMusic;

    SelectionBoyScript boyScript;
    SelectionGirlScript girlScript;
    EnteringHouse enteringHouseScript;
    RockScript rockScriptscript;

    GirlPlayerScript GirlPlayerScriptHacha;
    BoyPlayerScript BoyPlayerScriptHacha;

    public GameObject VideoObject;

    public GameObject Background;

    public GameObject CharacterSelectionBackground;

    public GameObject Level1Layer1;
    public GameObject Level1Layer2;
    public GameObject Level1Layer3;
    public GameObject Level1Layer4;
    public GameObject Level1Layer5;
    public GameObject Level1Layer6;

    public GameObject Level2layerPollution;

    public int LayersBool;

    public GameObject Chico;
    public GameObject Chica;
    public GameObject Pajaro;
    public GameObject Yayo;
    public GameObject Caja;
    public GameObject Roca;
    public GameObject Mineral;
    public GameObject RecogerTrigoGameObject;

    public GameObject Treee;

    public GameObject ChicoPlayer;
    public GameObject ChicaPlayer;

    public GameObject ChicoPlayerLogo;
    public GameObject ChicaPlayerLogo;

    public GameObject LevelBackground;
    public GameObject LevelForeground;
    public GameObject Year;

    public GameObject Level1Layer1House;
    public BoxCollider2D Level1Layer1HouseCollider;
    public BoxCollider2D level1Layer3Collider;

    public GameObject Level1Layer1HouseInterior;

    public GameObject YearText;

    public bool SaltoANivel2Bueno;
    public bool SaltoANivel2Malo;

    public bool SaltoANivel3MuyBueno;
    public bool SaltoANivel3Bueno;
    public bool SaltoANivel3Malo;
    public bool SaltoANivel3MuyMalo;

    public GameObject Final_0;
    public GameObject Final_30;
    public GameObject Final_60;
    public GameObject Final_100;

    public bool canMove;

    void Start()
    {
        timeScriptscript = GameObject.Find("GameObject11").GetComponent<TimeScript>();

        BackgroundMusic = GetComponent<AudioSource>();
        BackgroundMusic.clip = (AudioClip)Resources.Load("Outdoor_Ambiance", typeof(AudioClip));
        BackgroundMusic.Play();
        BackgroundMusic.GetComponent<AudioSource>().volume = 0;

        Video1 = GameObject.Find("Video").GetComponent<VideoPlayerScript>();
        TransitionGameObject.GetComponent<VideoPlayer>().Stop();

        boyScript = GameObject.Find("SelectionBoy").GetComponent<SelectionBoyScript>();
        girlScript = GameObject.Find("SelectionGirl").GetComponent<SelectionGirlScript>();
        enteringHouseScript = GameObject.Find("Casa_principal").GetComponent<EnteringHouse>();
        rockScriptscript = GameObject.Find("Roca").GetComponent<RockScript>();

        BoyPlayerScriptHacha = GameObject.Find("Chico").GetComponent<BoyPlayerScript>();
        GirlPlayerScriptHacha = GameObject.Find("Chica").GetComponent<GirlPlayerScript>();

        Background.transform.position = new Vector3(0.24f, 0.05f, 0);
        Background.SetActive(false);

        CharacterSelectionBackground.transform.position = new Vector3(-10.90f, 0.15f, 0);
        CharacterSelectionBackground.SetActive(false);

        Level1Layer1.SetActive(false);
        Level1Layer2.SetActive(false);
        Level1Layer3.SetActive(false);
        Level1Layer4.SetActive(false);
        Level1Layer5.SetActive(false);
        Level1Layer6.SetActive(false);
        Level1Layer1House.SetActive(false);

        Level1Layer1HouseCollider = Level1Layer1House.GetComponent<BoxCollider2D>();
        level1Layer3Collider = Level1Layer3.GetComponent<BoxCollider2D>();

        Level2layerPollution.SetActive(false);

        SaltoANivel2Bueno = false;
        SaltoANivel2Malo = false;

        RecogerTrigoGameObject.SetActive(false);

        SaltoANivel3MuyBueno = false;
        SaltoANivel3Bueno = false;
        SaltoANivel3Malo = false;
        SaltoANivel3MuyMalo = false;

        canMove = false;

        LayersBool = 1;
        Treee.SetActive(true);

        Chico.SetActive(false);
        Chica.SetActive(false);

        ChicaPlayer.SetActive(false);
        ChicoPlayer.SetActive(false);

        Caja.SetActive(false);
        Roca.SetActive(false);
        Mineral.SetActive(false);

        LevelBackground.SetActive(false);
        LevelForeground.SetActive(false);
        Year.SetActive(false);

        YearText.GetComponent<GUIText>();
        YearText.SetActive(false);

        ChicoPlayerLogo.SetActive(false);
        ChicaPlayerLogo.SetActive(false);

        Pajaro.SetActive(false);
        Yayo.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Video1.isVideoFinished == true)
        {
            Destroy(VideoObject);
            CharacterSelectionBackground.SetActive(true);

            Chica.transform.position = new Vector3(-4.5f, -1.7f, 0);
            Chico.transform.position = new Vector3(-17.89f, -2.02f, 0);

            Chica.SetActive(true);
            Chico.SetActive(true);

        }

        if (boyScript.BoySelected == true)
        {

            CharacterSelectionBackground.SetActive(false);
            Chica.SetActive(false);
            Chico.SetActive(false);

            if (Pajaro != null)
            {
                Pajaro.SetActive(true);
            }

            Yayo.SetActive(true);
            ChicoPlayer.SetActive(true);
            ChicoPlayerLogo.SetActive(true);

            BackgroundMusic.GetComponent<AudioSource>().volume = 1;

            if (Caja != null)
            {
                Caja.SetActive(true);
            }

            if (Roca != null)
            {
                Roca.SetActive(true);
            }

            LevelBackground.SetActive(true);
            LevelForeground.SetActive(true);
            Year.SetActive(true);

            YearText.SetActive(true);

            Chico.transform.position = new Vector3(-5.14f, -3.32f, 0);

            Level1Layer1.SetActive(true);
            Level1Layer2.SetActive(true);
            Level1Layer3.SetActive(true);
            Level1Layer4.SetActive(true);
            Level1Layer5.SetActive(true);
            Level1Layer6.SetActive(true);
            Level1Layer1House.SetActive(true);

        } else if (girlScript.GirlSelected == true)
        {

            CharacterSelectionBackground.SetActive(false);
            Chica.SetActive(false);
            Chico.SetActive(false);
            Pajaro.SetActive(true);
            Yayo.SetActive(true);
            ChicaPlayer.SetActive(true);
            ChicaPlayerLogo.SetActive(true);

            BackgroundMusic.GetComponent<AudioSource>().volume = 1;

            if (Caja != null)
            {
                Caja.SetActive(true); 
            }

            if (Roca != null)
            {
                Roca.SetActive(true);
            }

            LevelBackground.SetActive(true);
            LevelForeground.SetActive(true);
            Year.SetActive(true);

            YearText.SetActive(true);

            Chica.transform.position = new Vector3(-5.14f, -3.32f, 0);

            Level1Layer1.SetActive(true);
            Level1Layer2.SetActive(true);
            Level1Layer3.SetActive(true);
            Level1Layer4.SetActive(true);
            Level1Layer5.SetActive(true);
            Level1Layer6.SetActive(true);
            Level1Layer1House.SetActive(true);
        }

        if (timeScriptscript.Jump == 1)
        {
            if (SaltoANivel2Bueno == true)
            {
                Level1Layer1.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("NIVEL2/BUENA/Escenario2_Polucion0_Maiz01 (1)");
                Level1Layer2.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("NIVEL2/BUENA/Escenario2_Polucion0_02");
                Level1Layer3.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("NIVEL2/BUENA/Escenario2_Polucion0_03");
                Level1Layer4.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("NIVEL2/BUENA/Escenario2_Polucion0_04");
                Level1Layer5.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("NIVEL2/BUENA/Escenario2_Polucion0_05");
                Level1Layer6.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("NIVEL2/BUENA/Pantalla01_capa06");
                

                if (timeScriptscript.Years == 1900)
                {
                    timeScriptscript.Years = 2000;
                }

                Yayo.SetActive(false);

                if (BoyPlayerScriptHacha.ArbolPlantado == true || GirlPlayerScriptHacha.ArbolPlantado == true)
                {
                    Treee.transform.position = new Vector3(-14.02f, -3.78f, 0);
                }

                TransitionGameObject.GetComponent<VideoPlayer>().Play();
                StartCoroutine(TransitionVideoQuit());

                if (BoyPlayerScriptHacha.SemillasPlantado == true || GirlPlayerScriptHacha.SemillasPlantado == true)
                {
                    RecogerTrigoGameObject.SetActive(true);
                }
            }

            if (SaltoANivel2Malo == true)
            {
                Level1Layer1House.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("NIVEL2/MALA/Escenario2_Polucion100_CASA");
                Level1Layer1.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Escenario2_Polucion100_02 (1)");
                Level1Layer2.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("NIVEL2/MALA/Escenario2_Polucion100_03");
                Level1Layer3.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("NIVEL2/MALA/Escenario2_Polucion100_04");
                Level1Layer4.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("NIVEL2/MALA/Escenario2_Polucion100_05");
                Level1Layer5.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("NIVEL2/MALA/Escenario2_Polucion100_06");
                Level1Layer6.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("NIVEL2/MALA/Escenario2_Polucion100_07");

                if (timeScriptscript.Years == 1900)
                {
                    timeScriptscript.Years = 2000;
                }

                Level2layerPollution.SetActive(true);
                Yayo.SetActive(false);
                Mineral.SetActive(true);

                Destroy(Level1Layer1HouseCollider);
                Destroy(level1Layer3Collider);
                Level1Layer1House.transform.localScale = new Vector3(0.7f, 0.7f, 1.0f);

                if (BoyPlayerScriptHacha.ArbolPlantado == true || GirlPlayerScriptHacha.ArbolPlantado == true)
                {
                    Treee.transform.position = new Vector3(-14.02f, -3.78f, 0);
                }

                TransitionGameObject.GetComponent<VideoPlayer>().Play();
                StartCoroutine(TransitionVideoQuit());
            }
        }

        if (SaltoANivel3MuyMalo == true && Input.GetKeyDown(KeyCode.L))
        {
            canMove = true;
            Final_100.transform.position = new Vector3(CameraFinalScreen.transform.position.x, -6.54f, 1);
            Destroy(BackgroundMusic);
        }

        if (SaltoANivel3Malo == true && Input.GetKeyDown(KeyCode.L))
        {
            canMove = true;
            Final_60.transform.position = new Vector3(CameraFinalScreen.transform.position.x, -6.54f, 1);
            Destroy(BackgroundMusic);
        }

        if (SaltoANivel3MuyBueno == true && Input.GetKeyDown(KeyCode.L))
        {
            canMove = true;
            Final_0.transform.position = new Vector3(CameraFinalScreen.transform.position.x, -6.54f, 1);
            Destroy(BackgroundMusic);
        }

        if (SaltoANivel3Bueno == true && Input.GetKeyDown(KeyCode.L))
        {
            canMove = true;
            Final_30.transform.position = new Vector3(CameraFinalScreen.transform.position.x, -6.54f, 1);
            Destroy(BackgroundMusic);
        }

        if ((BoyPlayerScriptHacha.MineralExtraído == true || GirlPlayerScriptHacha.MineralExtraído == true) && (BoyPlayerScriptHacha.ArbolTalado == true || GirlPlayerScriptHacha.ArbolTalado == true))
        {
            SaltoANivel3MuyMalo = true;

        } else if ((BoyPlayerScriptHacha.MineralExtraído == true || GirlPlayerScriptHacha.MineralExtraído == true) && (BoyPlayerScriptHacha.ArbolPlantado == false || GirlPlayerScriptHacha.ArbolPlantado == false))
        {
            SaltoANivel3MuyMalo = true;
        }

        if ((BoyPlayerScriptHacha.MineralExtraído == true || GirlPlayerScriptHacha.MineralExtraído == true) && (BoyPlayerScriptHacha.ArbolPlantado == true || GirlPlayerScriptHacha.ArbolPlantado == true))
        {
            SaltoANivel3Malo = true;
        }

        if ((BoyPlayerScriptHacha.SemillasPlantado == true || GirlPlayerScriptHacha.SemillasPlantado == true) && (BoyPlayerScriptHacha.ArbolPlantado == true || GirlPlayerScriptHacha.ArbolPlantado == true) && (BoyPlayerScriptHacha.WheatCropGathered == true || GirlPlayerScriptHacha.WheatCropGathered == true))
        {
            SaltoANivel3MuyBueno = true;
        }

        if ((GirlPlayerScriptHacha.WheatCropGathered == false || BoyPlayerScriptHacha.WheatCropGathered == false) && timeScriptscript.Jump == 1 && (BoyPlayerScriptHacha.ArbolTalado == true || GirlPlayerScriptHacha.ArbolTalado == true))
        {
            SaltoANivel3Bueno = true;
        }

        if ((BoyPlayerScriptHacha.SemillasPlantado == true || GirlPlayerScriptHacha.SemillasPlantado == true) && rockScriptscript.RiverIsBlocked == false)
        {
            SaltoANivel2Bueno = true;
        }

        if (rockScriptscript.RiverIsBlocked == true && (BoyPlayerScriptHacha.HachaConseguida == true || GirlPlayerScriptHacha.HachaConseguida == true) && (BoyPlayerScriptHacha.PicoConseguido == true || GirlPlayerScriptHacha.PicoConseguido == true))
        {
            SaltoANivel2Malo = true;
        }

        if (canMove == true)
        {
            Final_100.transform.Translate(Vector3.up * 1 * Time.deltaTime, Space.World);
            timeScriptscript.Years = 2100;
            StartCoroutine(EndingImageDeletion());
        }

        if (canMove == true)
        {
            Final_60.transform.Translate(Vector3.up * 1 * Time.deltaTime, Space.World);
            timeScriptscript.Years = 2100;
            StartCoroutine(EndingImageDeletion());
        }

        if (canMove == true)
        {
            Final_0.transform.Translate(Vector3.up * 1 * Time.deltaTime, Space.World);
            timeScriptscript.Years = 2100;
            StartCoroutine(EndingImageDeletion());
        }

        if (canMove == true)
        {
            Final_30.transform.Translate(Vector3.up * 1 * Time.deltaTime, Space.World);
            timeScriptscript.Years = 2100;
            StartCoroutine(EndingImageDeletion());
        }
    }

    IEnumerator EndingImageDeletion()
    {
        yield return new WaitForSecondsRealtime(9.0f);
        Destroy(Final_0);
        Destroy(Final_100);
        Destroy(Final_30);
        Destroy(Final_60);
        Application.Quit();
    }

    IEnumerator TransitionVideoQuit()
    {
        yield return new WaitForSecondsRealtime(21.0f);
        TransitionGameObject.SetActive(false);
    }
}
