﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionGirlScript : MonoBehaviour
{
    public bool GirlSelected;

    // Start is called before the first frame update
    void Start()
    {
        GirlSelected = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            GirlSelected = true;
        }
    }
}
